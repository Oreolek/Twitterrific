all:
	mkdir -p build
	xelatex -interaction=nonstopmode -output-directory=build game.tex
	xelatex -interaction=nonstopmode -output-directory=build game.tex
watch:
	latexmk -pvc -pdf -jobname=build/game game.tex
